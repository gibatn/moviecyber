import { https } from "./configURL";
export let movieService = {
  getMovieList: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP09");
  },
  getMovieDetail: (id) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?maPhim=${id}`);
  },
  getMovieByTheater: () => {
    return https.get(
      "/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP09"
    );
  },
};

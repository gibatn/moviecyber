const USER = "USER";
export let localStorageServ = {
  user: {
    set: function (dataUser) {
      let dataJsonn = JSON.stringify(dataUser);
      localStorage.setItem(USER, dataJsonn);
    },
    get: function () {
      let dataJson = localStorage.getItem(USER);
      if (!dataJson) {
        return null;
      } else {
        return JSON.parse(dataJson);
      }
    },
    remove: function () {
      localStorage.removeItem(USER);
    },
  },
};

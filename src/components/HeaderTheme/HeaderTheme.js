import React from "react";
import UserNav from "./UserNav";
export default function HeaderTheme() {
  return (
    <div className="h-20 px-10 flex items-center justify-between shadow-lg shadow-red-100">
      <div style={{ color: "#003127" }} className="logo text-2xl">
        Logo
      </div>
      <UserNav />
    </div>
  );
}

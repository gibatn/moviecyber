import React from "react";

import { useSelector, useDispatch } from "react-redux";
import LoginPage from "../../Page/LoginPage/LoginPage";
import { loginAction } from "../../redux/action/userAction";
import { localStorageServ } from "../../services/localStorageService";
export default function UserNav() {
  let dispatch = useDispatch();
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  const renderContent = () => {
    if (userInfo) {
      return (
        <div className="flex space-x-5 items-center">
          <p>{userInfo.hoTen}</p>
          <button
            onClick={handleLogout}
            className="border-red-500 text-red-500 rounded px-5 py-3 border hover:bg-red-500 hover:text-blue-500"
          >
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="flex space-x-5 items-center">
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="border-blue-500 text-blue-500 rounded px-5 py-3 border"
          >
            Đăng nhập
          </button>
          <button className="border-red-500 text-red-500 rounded px-5 py-3 border">
            Đăng Ký
          </button>
        </div>
      );
    }
  };
  const handleLogout = () => {
    localStorageServ.user.remove();
    dispatch(loginAction(null));
  };
  return <div>{renderContent()}</div>;
}

import React from "react";
import { PacmanLoader } from "react-spinners";
import { useSelector } from "react-redux";
export default function SpinnerComponent() {
  let { isLoading } = useSelector((state) => state.spinnerReducer);
  return isLoading ? (
    <div
      style={{ backgroundColor: "#FFB200" }}
      className="h-screen w-screen fixed top-0 left-0 overflow-hidden flex justify-center items-center z-50"
    >
      <PacmanLoader color="#5ccf34" margin={5} size={75} />
    </div>
  ) : (
    ""
  );
}

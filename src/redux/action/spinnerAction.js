import { BAT_LOADING, TAT_LOADING } from "../constants/spinnerConstant";
export const batLoadingAction = () => ({
  type: BAT_LOADING,
});
export const tatLoadingAction = () => ({
  type: TAT_LOADING,
});

import { movieService } from "../../services/movie.service";
import { SET_MOVIE_LIST } from "../constants/movieConstants";

export const getMovieListActionService = () => {
  return (dispatch) => {
    movieService
      .getMovieList()
      .then((result) => {
        dispatch({
          type: SET_MOVIE_LIST,
          payload: result.data.content,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

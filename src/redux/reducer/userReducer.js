import { localStorageServ } from "../../services/localStorageService";

let initiState = {
  userInfo: localStorageServ.user.get(),
};
export let userReducer = (state = initiState, action) => {
  switch (action.type) {
    case "LOGIN": {
      state.userInfo = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};

import { SET_MOVIE_LIST } from "../constants/movieConstants";

const initialState = {
  movieList: [],
};

export const movieReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_MOVIE_LIST:
      return { ...state, movieList: payload };

    default:
      return state;
  }
};

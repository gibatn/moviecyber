import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../services/movie.service";
import { Progress } from "antd";
export default function DetailPage() {
  let { id } = useParams();
  const [movie, setmovie] = useState({});
  useEffect(() => {
    movieService
      .getMovieDetail(id)
      .then((result) => {
        setmovie(result.data.content);
      })
      .catch((error) => {});
  }, []);

  return (
    <div className="container mx-auto ">
      <div className="flex justify-center items-center p-10 space-x-10">
        <img className="w-60" src={movie.hinhAnh} alt="" />
        <p className="text-3xl font-medium text-red-500">{movie.biDanh}</p>
        <Progress
          type="circle"
          percent={movie.danhGia * 10}
          format={(number) => number / 10 + " điểm"}
        />
      </div>
    </div>
  );
}

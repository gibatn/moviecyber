import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieService } from "../../services/movie.service";
import ItemTabMovie from "./ItemTabMovie";
const { TabPane } = Tabs;
const onChange = (key) => {};
export default function TabMovie() {
  const [dataMovie, setdataMovie] = useState([]);
  useEffect(() => {
    movieService
      .getMovieByTheater()
      .then((result) => {
        setdataMovie(result.data.content);
      })
      .catch((error) => {});
  }, []);
  const renderContent = () => {
    return dataMovie.map((heThongRap, index) => {
      return (
        <TabPane
          tab={<img src={heThongRap.logo} className="w-10" />}
          key={index}
        >
          <Tabs
            style={{ height: 500 }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
          >
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <TabPane tab={renderTenCumRap(cumRap)} key={cumRap.maCumRap}>
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="shadow-lg"
                  >
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabMovie phim={phim} />;
                    })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };
  const renderTenCumRap = (cumRap) => {
    return (
      <div className="text-left w-40 ">
        <p className="text-black truncate">{cumRap.tenCumRap}</p>
        <p className="text-black truncate">{cumRap.diaChi}</p>
        <button className="w-full bg-red-400 rounded-lg">
          [ Xem chi tiết ]
        </button>
      </div>
    );
  };
  return (
    <div className="container mx-auto py-20 pb-96">
      {/* {isLoading ? <SpinnerComponent /> : ""} */}
      <Tabs
        style={{ height: 500 }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
      >
        {renderContent()}
      </Tabs>
    </div>
  );
}

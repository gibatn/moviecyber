import React, { useEffect } from "react";

import { useSelector, useDispatch } from "react-redux";
import { getMovieListActionService } from "../../redux/action/movieAction";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function ListMovie() {
  let { movieList } = useSelector((state) => {
    return state.movieReducer;
  });

  let dispatch = useDispatch();
  useEffect(() => {
    // movieService
    //   .getMovieList()
    //   .then((result) => {
    //
    //     setdata(10);
    //
    //   })
    //   .catch((error) => {
    //
    //   });
    dispatch(getMovieListActionService());
  }, []);
  let renderMovie = () => {
    return movieList
      .filter((item) => item.hinhAnh !== null)
      .map((item, index) => {
        return (
          <Card
            hoverable
            style={{
              width: "100%",
            }}
            className="hover:shadow-xl hover:shadow-slate-500 rounded-lg hover:scale-95 duration-500 transition"
            cover={
              <img alt="example" src={item.hinhAnh} className="rounded-lg" />
            }
          >
            <Meta
              title={<p className="text-blue-500 font-bold">{item.tenPhim}</p>}
              description={
                item.moTa.length > 50
                  ? item.moTa.substring(0, 50) + " . . ."
                  : item.moTa
              }
            />
            <NavLink to={`detail/${item.maPhim}`}>
              <button className="w-full bg-red-500 text-white rounded-lg text-xl font-medium">
                Mua vé
              </button>
            </NavLink>
          </Card>
        );
      });
  };

  return (
    <div className="grid grid-cols-4 gap-10 container mx-auto pt-5">
      {renderMovie()}
    </div>
  );
}

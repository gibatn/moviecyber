import { Button, Checkbox, Form, Input, message } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginAction } from "../../redux/action/userAction";
import { localStorageServ } from "../../services/localStorageService";
import { userService } from "../../services/user.service";
import LoginAnimate from "./LoginAnimate";

export default function LoginPage() {
  let dispatch = useDispatch();
  let history = useNavigate();
  const onFinish = (values) => {
    // history(-1);

    console.log("Success:", values);
    userService
      .postLogin(values)
      .then((result) => {
        console.log(result);
        message.success("Đăng nhập thành công");
        dispatch(loginAction(result.data.content));
        localStorageServ.user.set(result.data.content);
        //  về homepage nhưng load lại trang
        // window.location.href = "/";

        // không load lại trang
        setTimeout(() => {
          history("/");
        }, 1000);
      })
      .catch((error) => {
        console.log(error.response.data.content);
        message.error(`Đăng nhập thất bại ${error.response.data.content}`);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="bg-red-400 h-screen w-screen p-10">
      <div className="container h-full mx-auto bg-white p-10 rounded-xl flex">
        <div className="w-1/2 transform -translate-y-32 overflow-hidden">
          <LoginAnimate />
        </div>
        <div className="w-1/2">
          <Form
            name="basic"
            layout="vertical"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label={<p className="font-medium text-blue-700">Tài khoản</p>}
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tài khoản!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Mật khẩu"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <div className="flex justify-center">
              <button className="rounded px-5 py-2 text-white bg-red-700">
                Đăng nhập
              </button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}
